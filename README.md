# Run Database
1. run "docker-compose up --build"
2. open browser for phpmyadmin "http://localhost:5001"
3. user "root" pass "root"

# Create Database Name
1. "customer" and import file /initial/{last_floder or last_sprint}/customer.sql
2. "portal" and import file /initial/{last_floder or last_sprint}/portal.sql