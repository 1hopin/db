CREATE TABLE admin_admin.languages (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_slug` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

INSERT INTO admin_admin.languages (`language_id`, `language_name`, `language_slug`) VALUES
(1, 'Thailand', 'th');

CREATE TABLE admin_admin.member (
  `member_id` int(11) NOT NULL,
  `group_id` smallint(2) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `telephone` varchar(10) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_on` datetime DEFAULT NULL,
  `login_on` datetime DEFAULT NULL,
  `logout_on` datetime DEFAULT NULL,
  `update_on` datetime DEFAULT NULL,
  `change_password_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

INSERT INTO admin_admin.member (`member_id`, `group_id`, `parent_id`, `username`, `password`, `salt`, `name`, `lastname`, `telephone`, `location_id`, `status`, `create_on`, `login_on`, `logout_on`, `update_on`, `change_password_on`) VALUES
(1, 1, 0, 'nick', '4823105c4f01b8cb0cae40d4935cfaeb', 'f79b243766171f9417c50dd20e2f1b0d', 'Suttipong', 'Ruanglitt', NULL, 0, 1, '2020-03-26 00:00:00', '2022-05-16 08:02:54', '2022-05-16 08:26:33', '2022-05-16 08:26:33', '2022-04-20 17:57:59'),
(20, 2, 0, 'admin', 'fe3d1a0c207a599895a80463bf7936fd', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '1hopin', '0908888888', NULL, 1, '2022-05-16 08:26:07', '2022-05-16 08:26:46', NULL, '2022-05-16 08:26:46', NULL);


CREATE TABLE admin_admin.member_group (
  `group_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `allow_menu` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

INSERT INTO admin_admin.member_group (`group_id`, `name`, `allow_menu`) VALUES
(1, 'Developer', NULL),
(2, 'Admin', '[\"15\",\"17\"]');

CREATE TABLE admin_admin.menu (
  `menu_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon_name` varchar(255) DEFAULT NULL,
  `is_toggle` tinyint(1) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

INSERT INTO admin_admin.menu (`menu_id`, `slug`, `name`, `url`, `icon_name`, `is_toggle`, `parent`, `order`) VALUES
(1, 'dashboard', 'Dashboard', NULL, '1.png', 0, 0, 1),
(2, 'member', 'Member', NULL, '5.png', 1, 0, 5),
(3, 'add_member', 'Add Member', 'member/add', NULL, 0, 2, 1),
(4, 'manage_member', 'Manage Member', 'member/manage', NULL, 0, 2, 1),
(5, 'group_member', 'Group Member', 'member/group/list', NULL, 0, 2, 2),
(15, 'customers', 'Customer', NULL, '2.png', 1, 0, 3),
(17, 'manage_customers', 'Manage Customer', 'customers/manage', NULL, 0, 15, 1);

ALTER TABLE admin_admin.languages
  ADD PRIMARY KEY (`language_id`);

ALTER TABLE admin_admin.member
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD KEY `username` (`username`),
  ADD KEY `salt` (`salt`),
  ADD KEY `telephone` (`telephone`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `status` (`status`),
  ADD KEY `location_id` (`location_id`);

ALTER TABLE admin_admin.member_group
  ADD PRIMARY KEY (`group_id`);

ALTER TABLE admin_admin.menu
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `url` (`url`),
  ADD KEY `name` (`name`),
  ADD KEY `parent` (`parent`) USING BTREE,
  ADD KEY `slug` (`slug`);

ALTER TABLE admin_admin.languages
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE admin_admin.member
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

ALTER TABLE admin_admin.member_group
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE admin_admin.menu
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;