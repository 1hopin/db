CREATE TABLE admin_customer.customers (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `create_on` int(11) NOT NULL,
  `display_status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

INSERT INTO admin_customer.customers (`id`, `firstname`, `lastname`, `phone_no`, `create_on`, `display_status`) VALUES
(14, 'Nicky', 'Nakrub', '0909861363', 1652946827, 1),
(15, 'Suttipong', 'Ruanglitt', '0909861366', 1652946922, 1);

ALTER TABLE admin_customer.customers
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_no` (`phone_no`),
  ADD KEY `display_status` (`display_status`);

ALTER TABLE admin_customer.customers
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
